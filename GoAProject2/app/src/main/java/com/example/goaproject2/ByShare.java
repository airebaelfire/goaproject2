package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class ByShare extends AppCompatActivity {

    public static ArrayList<Double> splitMultiplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_by_share);

        Button finishButton = (Button) findViewById(R.id.finishButton);
        finishButton.setTransformationMethod(null);
        ImageButton backButton = (ImageButton) findViewById(R.id.backButton);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        shareAdapter share = new shareAdapter(finishButton);
        TextView errorMessage = findViewById(R.id.errorMessage);
        TextView somethingIsEmptyMessage = findViewById(R.id.somethingIsEmptyMessage);

        errorMessage.setVisibility(View.GONE);
        somethingIsEmptyMessage.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(share);

        backButton.setOnClickListener(v -> {
            //Go back
            finish();
        });


        finishButton.setOnClickListener(v -> {

            splitMultiplier = new ArrayList<>();
            double number = 0;
            boolean isNotEmpty = true;
            int defaultTotal = GlobalResources.people.stream().mapToInt(person -> person.amount).sum();
            for (int k = 0; k < shareAdapter.numerator.size(); k++) {
                System.out.println(shareAdapter.numerator.get(k));
                System.out.println(shareAdapter.denominator.get(k));

                if (shareAdapter.numerator.get(k).equals("") || shareAdapter.denominator.get(k).equals("")) {

                    somethingIsEmptyMessage.setVisibility(View.VISIBLE);
                    errorMessage.setVisibility(View.GONE);
                    isNotEmpty = false;
                    break;

                }

                int num = Integer.parseInt(shareAdapter.numerator.get(k));
                int dem = Integer.parseInt(shareAdapter.denominator.get(k));



                splitMultiplier.add((double) num / dem);
                number += splitMultiplier.get(k);
            }

            if (number == 1 && isNotEmpty != false) {

                System.out.println("Success!");
                errorMessage.setVisibility(View.GONE);
                somethingIsEmptyMessage.setVisibility(View.GONE);

                //Leave Spinner screen
                for (int i = 0; i < shareAdapter.numerator.size(); i++) {
                    double numeratorx = Integer.parseInt(shareAdapter.numerator.get(i));
                    double denominatorx = Integer.parseInt(shareAdapter.denominator.get(i));
                    GlobalResources.people.get(i).setAmount((int)((double) defaultTotal * (numeratorx/denominatorx)));
                    double diff = defaultTotal - (((int)((double) defaultTotal * (numeratorx/denominatorx))) * (denominatorx/numeratorx));
                    GlobalResources.people.get((int)(Math.random()*GlobalResources.people.size())).amount += diff;
                }
                Intent intent = new Intent(ByShare.this, FinalSummary.class);
                startActivity(intent);
            }
            else if (isNotEmpty == false){

                errorMessage.setVisibility(View.GONE);
                somethingIsEmptyMessage.setVisibility(View.VISIBLE);
            }
            else {

                errorMessage.setVisibility(View.VISIBLE);
                somethingIsEmptyMessage.setVisibility(View.GONE);
            }

        });
    }
}