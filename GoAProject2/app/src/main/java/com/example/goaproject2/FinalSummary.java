package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Button;

public class FinalSummary extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_summary);
        FinalPersonAdapter finalPersonAdapter = new FinalPersonAdapter(GlobalResources.people, this);
        RecyclerView personRecyclerView = findViewById(R.id.final_recyclerView);
        personRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        personRecyclerView.setAdapter(finalPersonAdapter);

        Button back_button = findViewById(R.id.final_back_button);
        back_button.setOnClickListener(v -> finish());
    }
}