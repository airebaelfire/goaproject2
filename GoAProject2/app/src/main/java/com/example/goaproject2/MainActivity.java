package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    SwitchCompat dark_light;
    static ConstraintLayout mainScreen;
    static LayerDrawable mainBack;
    static PersonAdapter personAdapter;
    static Button splitBillButton;
    static Button editPeopleButton;
    static ImageView splitBill;
    static ImageView camera;

    private static int blue_dark;
    private static int blue_light;
    private static int yellow_dark;
    private static int white;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainScreen = findViewById(R.id.main_screen);
        mainBack = ((LayerDrawable) Objects.requireNonNull(ContextCompat.getDrawable(this, R.drawable.main_back)));
        blue_dark = getColor(R.color.blue_dark);
        blue_light = getColor(R.color.blue_light);
        yellow_dark = getColor(R.color.yellow_dark);
        white = getColor(R.color.white);

        dark_light = findViewById(R.id.dark_light);
        dark_light.setChecked(GlobalResources.mode == Display_Mode.DARK);
        dark_light.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            GlobalResources.toggleMode(isChecked);
            Toast.makeText(this, "Dark Mode coming soon!", Toast.LENGTH_SHORT).show();
            dark_light.toggle();
        });

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        personAdapter = new PersonAdapter(GlobalResources.people, this, size.x - 60);
        RecyclerView personRecyclerView = findViewById(R.id.people_recycler_view);
        personRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        personRecyclerView.setAdapter(personAdapter);

        splitBillButton = findViewById(R.id.split_bill_button);
        splitBillButton.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, choose_method.class);
            startActivity(intent);
        });
        editPeopleButton = findViewById(R.id.edit_people_button);
        editPeopleButton.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity.this, EditMainList.class);
            startActivity(i);
        });
        splitBill = findViewById(R.id.bird);
        camera = findViewById(R.id.camera);
        camera.setOnClickListener(v -> {
            Intent i = new Intent(MainActivity.this, Receipt_Scanner.class);
            startActivity(i);
        });
    }

    public static void onChangeMode(Display_Mode mode) {
        switch (mode) {
            case LIGHT:
                ValueAnimator light_colorAnimation = ValueAnimator.ofArgb(blue_dark, blue_light);
                light_colorAnimation.setDuration(250);
                light_colorAnimation.addUpdateListener(animation -> {
                    ((GradientDrawable) mainBack.findDrawableByLayerId(R.id.main_back_color)).setColor((int) animation.getAnimatedValue());
                    mainScreen.setBackground(mainBack);
                });
                ValueAnimator lightImage_colorAnimation = ValueAnimator.ofArgb(yellow_dark, white);
                lightImage_colorAnimation.setDuration(250);
                lightImage_colorAnimation.addUpdateListener(animation -> {
                    ((BitmapDrawable) mainBack.findDrawableByLayerId(R.id.main_back_image)).setColorFilter((int) animation.getAnimatedValue(), PorterDuff.Mode.SRC_ATOP);
                });
                light_colorAnimation.start();
                lightImage_colorAnimation.start();
                ((BitmapDrawable) mainBack.findDrawableByLayerId(R.id.main_back_image)).setColorFilter(null);
                personAdapter.notifyDataSetChanged();
                splitBillButton.setBackgroundTintList(ContextCompat.getColorStateList(GlobalResources.context, R.color.yellow_light));
                editPeopleButton.setBackground(ContextCompat.getDrawable(GlobalResources.context, R.drawable.yellow_back_shadow));
                editPeopleButton.setTextColor(ContextCompat.getColor(GlobalResources.context, R.color.black));
                splitBill.setImageDrawable(ContextCompat.getDrawable(GlobalResources.context, R.drawable.split_bill_light));
                camera.setImageDrawable(ContextCompat.getDrawable(GlobalResources.context, R.drawable.camera_dark));
                break;
            case DARK:
                ValueAnimator dark_colorAnimation = ValueAnimator.ofArgb(blue_light, blue_dark);
                dark_colorAnimation.setDuration(250);
                dark_colorAnimation.addUpdateListener(animation -> {
                    ((GradientDrawable) mainBack.findDrawableByLayerId(R.id.main_back_color)).setColor((int) animation.getAnimatedValue());
                    mainScreen.setBackground(mainBack);
                });
                ValueAnimator darkImage_colorAnimation = ValueAnimator.ofArgb(white, yellow_dark);
                darkImage_colorAnimation.setDuration(250);
                darkImage_colorAnimation.addUpdateListener(animation -> {
                    ((BitmapDrawable) mainBack.findDrawableByLayerId(R.id.main_back_image)).setColorFilter((int) animation.getAnimatedValue(), PorterDuff.Mode.SRC_ATOP);
                });
                dark_colorAnimation.start();
                darkImage_colorAnimation.start();
                personAdapter.notifyDataSetChanged();
                splitBillButton.setBackgroundTintList(ContextCompat.getColorStateList(GlobalResources.context, R.color.yellow_dark));
                editPeopleButton.setBackground(ContextCompat.getDrawable(GlobalResources.context, R.drawable.darkblue_back_shadow));
                editPeopleButton.setTextColor(ContextCompat.getColor(GlobalResources.context, R.color.white));
                splitBill.setImageDrawable(ContextCompat.getDrawable(GlobalResources.context, R.drawable.split_bill_dark));
                camera.setImageDrawable(ContextCompat.getDrawable(GlobalResources.context, R.drawable.camera_light));
                break;
        }
    }
}