package com.example.goaproject2;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ViewHolder> {
    ArrayList<Person> people;
    Context context;
    private final int screenWidth;
    ItemEditAdapter itemEditAdapter;

    public PersonAdapter(ArrayList<Person> people, Context context, int screenWidth) {
        this.people = people;
        this.context = context;
        this.screenWidth = screenWidth;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout back;
        TextView name;
        TextView amount;
        ImageView edit;
        Button add;

        RecyclerView itemsView;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.person_name);
            amount = view.findViewById(R.id.person_amount);
            back = view.findViewById(R.id.person_back);
            edit = view.findViewById(R.id.person_edit);
            itemsView = view.findViewById(R.id.person_items_recyclerview);
            add = view.findViewById(R.id.person_add_item);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.person, viewGroup, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.name.setText(people.get(position).name);
        people.get(position).amount = 0;
        people.get(position).items.forEach(item -> people.get(position).amount += item.price);
//        people.get(position).open = false;
        String amount = "$" + new DecimalFormat("#.00").format((double)people.get(position).amount/100);
        viewHolder.amount.setText(amount);
        ItemAdapter itemAdapter = new ItemAdapter(people.get(position).items);
        viewHolder.itemsView.setLayoutManager(new LinearLayoutManager(context));
        viewHolder.itemsView.setAdapter(itemAdapter);
        switch (GlobalResources.mode) {
            case DARK:
                viewHolder.back.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.blue_light));
                viewHolder.itemsView.setBackground(ContextCompat.getDrawable(context, R.drawable.person_items_back_light));
                viewHolder.name.setTextColor(ContextCompat.getColorStateList(context, R.color.black));
                break;
            case LIGHT:
                viewHolder.back.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.blue_dark));
                viewHolder.itemsView.setBackground(ContextCompat.getDrawable(context, R.drawable.person_items_back_dark));
                viewHolder.name.setTextColor(ContextCompat.getColorStateList(context, R.color.white));
                break;
        }
        if (people.get(position).open) {
            viewHolder.back.setBackground(AppCompatResources.getDrawable(context, R.drawable.person_back_selected));
            viewHolder.add.setVisibility(View.VISIBLE);
        } else if (!(people.get(position).open)) {
            viewHolder.back.setBackground(AppCompatResources.getDrawable(context, R.drawable.person_back));
            viewHolder.add.setVisibility(View.GONE);
        }
        viewHolder.back.setOnClickListener(v -> {
            if (people.get(position).open) {
                viewHolder.back.setBackground(AppCompatResources.getDrawable(context, R.drawable.person_back));
                viewHolder.add.setVisibility(View.GONE);
            } else if (!(people.get(position).open)) {
                viewHolder.back.setBackground(AppCompatResources.getDrawable(context, R.drawable.person_back_selected));
                viewHolder.add.setVisibility(View.VISIBLE);
            }
            SlideAnimation slideAnimation = new SlideAnimation(viewHolder.itemsView, 300);
            viewHolder.itemsView.startAnimation(slideAnimation);
            System.out.println(viewHolder.back.getBackground());
            people.get(position).open = !people.get(position).open;
        });
        viewHolder.add.setOnClickListener(v -> {
            DialogFragment dialog = new CustomAlertDialog(people.get(position));
            dialog.show(((FragmentActivity)context).getSupportFragmentManager(), "Add");
        });
        viewHolder.edit.setOnClickListener(v -> {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = inflater.inflate(R.layout.popup_window, null);
            TextView namePopup = popupView.findViewById(R.id.popup_name);
            namePopup.setText(people.get(position).name);
            TextView amountPopup = popupView.findViewById(R.id.popup_amount);
            amountPopup.setText(amount);
            RecyclerView itemsPopup = popupView.findViewById(R.id.popup_items_recyclerview);
            itemsPopup.setLayoutManager(new LinearLayoutManager(context));
            itemEditAdapter = new ItemEditAdapter(people.get(position), context);
            itemsPopup.setAdapter(itemEditAdapter);
            switch (GlobalResources.mode) {
                case DARK:
                    popupView.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.blue_light));
                    ((TextView) popupView.findViewById(R.id.popup_name)).setTextColor(context.getColor(R.color.black));
                    ((TextView) popupView.findViewById(R.id.popup_amount)).setTextColor(context.getColor(R.color.black));
                    ((Button) popupView.findViewById(R.id.popup_done)).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.yellow_dark));
                    ((RecyclerView) popupView.findViewById(R.id.popup_items_recyclerview)).setBackground(ContextCompat.getDrawable(context, R.drawable.person_items_back_light));
                    break;
                case LIGHT:
                    popupView.setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.blue_dark));
                    ((TextView) popupView.findViewById(R.id.popup_name)).setTextColor(context.getColor(R.color.white));
                    ((TextView) popupView.findViewById(R.id.popup_amount)).setTextColor(context.getColor(R.color.white));
                    ((Button) popupView.findViewById(R.id.popup_done)).setBackgroundTintList(ContextCompat.getColorStateList(context, R.color.yellow_light));
                    ((RecyclerView) popupView.findViewById(R.id.popup_items_recyclerview)).setBackground(ContextCompat.getDrawable(context, R.drawable.person_items_back_dark));
                    break;
            }
            int height = LinearLayout.LayoutParams.WRAP_CONTENT;
            boolean focusable = true;
            final PopupWindow popupWindow = new PopupWindow(popupView, screenWidth, height, focusable);
            popupWindow.showAtLocation(viewHolder.back, Gravity.CENTER, 0, 0);
            popupView.findViewById(R.id.popup_done).setOnClickListener(v1 -> {
                for (int i = 0; i < people.get(position).items.size(); i++) {
                    ItemEditAdapter.ViewHolder viewHolder1 = (ItemEditAdapter.ViewHolder) itemsPopup.findViewHolderForAdapterPosition(i);
                    people.get(position).items.get(i).name = Objects.requireNonNull(viewHolder1).name.getText().toString();
                    people.get(position).items.get(i).price = Integer.parseInt(Objects.requireNonNull(viewHolder1).price.getText().toString().replaceAll("[^0-9]", ""));
                }
                itemAdapter.notifyItemRangeChanged(0, people.get(position).items.size());
                itemEditAdapter.notifyItemRangeChanged(0, people.get(position).items.size());;
                notifyItemRangeChanged(0, people.size());
                popupWindow.dismiss();
            });
        });
    }


    @Override
    public int getItemCount() {
        return people.size();
    }
}
