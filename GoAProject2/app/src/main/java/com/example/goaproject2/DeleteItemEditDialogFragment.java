package com.example.goaproject2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class DeleteItemEditDialogFragment extends DialogFragment {
    Item item;
    Person person;

    public DeleteItemEditDialogFragment(Item item, Person person) {
        this.item = item;
        this.person = person;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete " + item.name + "?")
                .setPositiveButton("Yes", (dialog, id) -> {
                    person.items.remove(item);
                    MainActivity.personAdapter.notifyDataSetChanged();
                    MainActivity.personAdapter.itemEditAdapter.notifyDataSetChanged();
                })
                .setNegativeButton("No! Go back!", (dialog, id) -> {
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}