package com.example.goaproject2;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class SlideAnimation extends Animation {
    private View viewToBeAnimated;
    private ViewGroup.MarginLayoutParams viewMarginParams;
    private boolean hideAfter;
    private int marginStart;
    private int marginEnd;
    private boolean alreadyFinished = false;

    public SlideAnimation(View view, int duration) {
        //Set the duration of the animation to the int we passed in
        setDuration(duration);

        //Set the view to be animated to the view we passed in
        viewToBeAnimated = view;

        //Get the Margin Parameters for the view so we can edit them
        viewMarginParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();

        //If the view is VISIBLE, hide it after. If it's GONE, show it before we start.
        hideAfter = (view.getVisibility() == View.VISIBLE);

        //First off, start the margin at the bottom margin we've already set.
        //You need your layout to have a negative margin for this to work correctly.
        marginStart = viewMarginParams.bottomMargin;

        //Decide if we're expanding or collapsing
        if (marginStart == 0){
            marginEnd = 0 - view.getHeight();
        }
        else {
            marginEnd = 0;
        }

        //Make sure the view is visible for our animation
        view.setVisibility(View.VISIBLE);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {

            // Setting the new bottom margin to the start of the margin
            // plus the inbetween bits
            viewMarginParams.bottomMargin = marginStart + (int) ((marginEnd - marginStart) * interpolatedTime);

            // Request the layout as it happens so we can see it redrawing
            viewToBeAnimated.requestLayout();

            // Make sure we have finished before we mess about with the rest of it
        } else if (!alreadyFinished) {
            viewMarginParams.bottomMargin = marginEnd;
            viewToBeAnimated.requestLayout();

            if (hideAfter) {
                viewToBeAnimated.setVisibility(View.GONE);
            }
            alreadyFinished = true;
        }
        hideAfter = false;
    }
}
