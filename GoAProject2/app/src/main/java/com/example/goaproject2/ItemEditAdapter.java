package com.example.goaproject2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class ItemEditAdapter extends RecyclerView.Adapter<ItemEditAdapter.ViewHolder>{
    ArrayList<Item> items;
    Person person;
    Context context;

    public ItemEditAdapter(Person person, Context context) {
        this.person = person;
        this.items = person.items;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        EditText name;
        EditText price;
        Button trash;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_name);
            price = view.findViewById(R.id.item_price);
            trash = view.findViewById(R.id.item_trash);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemedit, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.name.setText(items.get(position).name);
        String price = "$" + new DecimalFormat("#.00").format((double)items.get(position).price/100);
        viewHolder.price.setText(price);
        viewHolder.trash.setVisibility(View.VISIBLE);
        viewHolder.trash.setOnClickListener(v -> new DeleteItemEditDialogFragment(items.get(position), person).show(((FragmentActivity)context).getSupportFragmentManager(), "delete"));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
