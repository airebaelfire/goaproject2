package com.example.goaproject2;

import android.app.Application;
import android.content.Context;

import java.util.ArrayList;
import java.util.Arrays;

enum Display_Mode {
    DARK, LIGHT
}

public class GlobalResources extends Application {
    public static Context context;
    public static ScannerItemAdapter scannerItemAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        GlobalResources.context = getApplicationContext();
    }

    public static Display_Mode mode = Display_Mode.LIGHT;
    public static ArrayList<Person> people = new ArrayList<>(Arrays.asList(
            new Person("1", 0, new ArrayList<>(), false)
    ));

    public static void toggleMode(boolean toggle) {
        mode = toggle ? Display_Mode.DARK : Display_Mode.LIGHT;
        MainActivity.onChangeMode(mode);
    }
}