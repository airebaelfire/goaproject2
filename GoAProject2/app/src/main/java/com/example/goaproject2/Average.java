package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class Average extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.average);

        ImageView back_button_average = findViewById(R.id.back_button_average) ;
        ImageView legs = findViewById(R.id.legs) ;
        TextView title_average =  findViewById(R.id.title_average) ;
        ImageView blue_rect = findViewById(R.id.blue_rect) ;
        ImageView grey_rect = findViewById(R.id.gray_rect) ;
        TextView total_amount_text =  findViewById(R.id.total_amount) ;
        ImageView lit_rect_1 = findViewById(R.id.lit_rect1) ;
        ImageView lit_rect2 = findViewById(R.id.lit_rect2) ;
        ImageView dot_average = findViewById(R.id.dot) ;
        ImageView finish_button_average = findViewById(R.id.finish_button_average);
        TextView finish_button_text_average = findViewById(R.id.finish_text_average) ;
        ImageView arrow_average = findViewById(R.id.arrow_average) ;
        EditText edit_number_average_1 = findViewById(R.id.edit_number_average_1);
        EditText edit_number_average_2 = findViewById(R.id.edit_number_average_2);
        TextView dollar_sign_average = findViewById(R.id.dollar_sign_average) ;


        int defaultTotal = GlobalResources.people.stream().mapToInt(person -> person.amount).sum();
        edit_number_average_1.setText(Integer.toString(defaultTotal/100));
        edit_number_average_2.setText(new DecimalFormat("00").format((defaultTotal)%100));


        back_button_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        finish_button_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_number_average_1.getText().toString().isEmpty() || edit_number_average_2.getText().toString().isEmpty()) {
                    Toast.makeText(Average.this, "Please enter an amount.", Toast.LENGTH_SHORT).show();
                } else if (edit_number_average_2.getText().toString().length() > 2) {
                    Toast.makeText(Average.this, "Please enter a valid amount.", Toast.LENGTH_SHORT).show();
                } else {
                    String totalAmount = edit_number_average_1.getText().toString() + edit_number_average_2.getText().toString();
                    int totalMoney = Integer.parseInt(totalAmount);
                    int totalPeople = GlobalResources.people.size();
                    if (((int)(totalMoney/totalPeople))*totalPeople == totalMoney) {
                        GlobalResources.people.forEach(person -> person.amount = totalMoney / totalPeople);
                    } else {
                        double diff = totalMoney - (((int)(totalMoney/totalPeople))*totalPeople);
                        GlobalResources.people.forEach(person -> person.amount = totalMoney / totalPeople);
                        GlobalResources.people.get((int)(Math.random()*GlobalResources.people.size())).amount += diff;
                    }
                    Intent intent = new Intent(Average.this, FinalSummary.class);
                    startActivity(intent);
                }
            }
        });
    }
}