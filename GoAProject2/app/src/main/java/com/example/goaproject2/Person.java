package com.example.goaproject2;

import java.util.ArrayList;

public class Person {
    public String name;
    public int amount;
    public ArrayList<Item> items;
    public boolean open;

    public Person(String name, int amount, ArrayList<Item> items, boolean open) {
        this.name = name;
        this.amount = amount;
        this.items = items;
        this.open = open;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return name;
    }
}
