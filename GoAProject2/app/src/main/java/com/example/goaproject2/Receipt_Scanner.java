package com.example.goaproject2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

public class Receipt_Scanner extends AppCompatActivity {

    private ImageView captureIV;
    private TextView resultTV;
    private Button snapBtn, chooseBtn;
    private Button rotate1, rotate2;
    private ImageView popupClose;
    private Button popupNext, done;
    private FrameLayout frameLayout;
    private View editScannerLayout;
    private Bitmap imageBitmap;
    ConstraintLayout background;
    private Uri imageUri;
    public static TreeMap<String,String> items = new TreeMap<>();
    private StringBuffer itemsString = new StringBuffer(100);
    private boolean isProcessing = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri result) {
            if (result != null) {
                imageUri = result;
                captureIV.setImageURI(result);
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                rotate1.setVisibility(View.VISIBLE);
                rotate2.setVisibility(View.VISIBLE);
                if (!isProcessing) {
                    detectText();
                }
            }
        }
    });

    private final ActivityResultLauncher<Void> mTakePicture = registerForActivityResult(new ActivityResultContracts.TakePicturePreview(), getActivityResultRegistry(), new ActivityResultCallback<Bitmap>() {
        @Override
        public void onActivityResult(Bitmap bitmap) {
            if (bitmap != null) {
                imageBitmap = bitmap;
                captureIV.setImageBitmap(bitmap);
//            Images.Media.insertImage(getContentResolver(), bitmap, "receipt", "receipt for " + System.currentTimeMillis());
                rotate1.setVisibility(View.VISIBLE);
                rotate2.setVisibility(View.VISIBLE);
                if (!isProcessing) {
                    detectText();
                }
            }
        }
    });

    private ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
        if (isGranted) {
            mTakePicture.launch(null);
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_scanner);

        captureIV = findViewById(R.id.idIVCaptureImage);
        resultTV = findViewById(R.id.idIVDetectedText);
        snapBtn = findViewById(R.id.idBtnSnap);
        chooseBtn = findViewById(R.id.idBtnChoose);
//        detectBtn = findViewById(R.id.idBtnDetect);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        editScannerLayout = findViewById(R.id.editScannerLayout);
        background = findViewById(R.id.receiptScanner_background);
        switch (GlobalResources.mode) {
            case LIGHT:
                background.setBackgroundColor(getColor(R.color.blue_light));
                break;
            case DARK:
                background.setBackgroundColor(getColor(R.color.blue_dark));
                break;
        }

        rotate1 = findViewById(R.id.rotate1);
        rotate1.setVisibility(View.GONE);
        rotate1.setOnClickListener(v -> {
            userRotateImage(1);
            captureIV.setImageBitmap(imageBitmap);
        });

        rotate2 = findViewById(R.id.rotate2);
        rotate2.setVisibility(View.GONE);
        rotate2.setOnClickListener(v -> {
            userRotateImage(2);
            captureIV.setImageBitmap(imageBitmap);
        });

        popupClose = findViewById(R.id.closeEditScanner);
        popupClose.setOnClickListener(v -> editScannerLayout.setVisibility(View.GONE));

        done = findViewById(R.id.scanner_done);
        done.setOnClickListener(v -> editScannerLayout.setVisibility(View.VISIBLE));

// detectBtn.setOnClickListener(v -> {
//     if (!isProcessing) {
//         detectText();
//     }
// });

        snapBtn.setOnClickListener(v -> {
//                if(checkPermissions()) {
//                    captureImage();
//                } else {
//                    requestPermission();
//                }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                mTakePicture.launch(null);
            } else {
                requestPermissionLauncher.launch(Manifest.permission.CAMERA);
            }
        });

        chooseBtn.setOnClickListener(v -> {
            mGetContent.launch("image/*");
        });

        popupNext = findViewById(R.id.editScannerNext);
        popupNext.setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    protected void onDestroy() {
        MainActivity.onChangeMode(GlobalResources.mode);
        super.onDestroy();
    }

    //    private boolean checkPermissions() {
//        int cameraPermission = ContextCompat.checkSelfPermission(getApplicationContext(),CAMERA);
//        return cameraPermission == PackageManager.PERMISSION_GRANTED;
//    }
//
//    private void requestPermission() {
//        int PERMISSION_CODE = 200;
//        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, PERMISSION_CODE);
//    }
//
//    private void captureImage() {
//        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if(takePicture.resolveActivity(getPackageManager()) != null) {
//            startActivityForResult(takePicture, REQUEST_IMAGE_CAPTURE);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if(grantResults.length > 0) {
//            boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//            if(cameraPermission) {
//                Toast.makeText(this, "Permissions Granted..", Toast.LENGTH_SHORT).show();
//                captureImage();
//            } else {
//                Toast.makeText(this, "Permissions Denied..", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            imageBitmap = (Bitmap) extras.get("data");
//            captureIV.setImageBitmap(imageBitmap);
//            rotate1.setVisibility(View.VISIBLE);
//            rotate2.setVisibility(View.VISIBLE);
//        }
//    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void userRotateImage(int direction) {
        if (direction == 1) {
            imageBitmap = rotateImage(imageBitmap, 90);
//            if (!isProcessing) {
//                detectText();
//            }
        } else if (direction == 2) {
            imageBitmap = rotateImage(imageBitmap, 270);
//            if (!isProcessing) {
//                detectText();
//            }
        }
    }

    private void detectText() {
        isProcessing = true;
        InputImage image = InputImage.fromBitmap(imageBitmap, 0);
//        InputImage image = null;
//        try {
//            image = InputImage.fromFilePath(getApplicationContext(), imageUri);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
        Task<Text> result = recognizer.process(image).addOnSuccessListener(text -> {
            StringBuilder result1 = new StringBuilder();
            ArrayList<String> resultArray = new ArrayList<>();
            ArrayList<Rect> rectArray = new ArrayList<>();
            for(Text.TextBlock block : text.getTextBlocks()) {
                String blockText = block.getText();
                Point[] blockCornerPoint = block.getCornerPoints();
                Rect blockFrame = block.getBoundingBox();
                for(Text.Line line : block.getLines()) {
                    String lineText = line.getText();
                    Point[] lineCornerPoint = line.getCornerPoints();
                    Rect linRect = line.getBoundingBox();
//                    Draw(linRect.left, linRect.top, linRect.right, linRect.bottom);
//                    System.out.println(lineText);
                    for(Text.Element element : line.getElements()) {
                        String elementText = element.getText();
                        result1.append(elementText); //*Note: Might want to add spaces after each
//                            resultArray.add(elementText);
//                            System.out.println(elementText);
                    }
//                        resultTV.setText(blockText);
//                        System.out.println(lineText);
//                        analizeText(lineText);
                    resultArray.add(lineText);
                    rectArray.add(linRect);
//                        resultTV.setText(String.join(", ", analizeText(lineText)));
                }
            }
            items = analyzeText(resultArray, rectArray);
            itemsString = new StringBuffer(100);
            for (int i = 0; i < items.size(); i++) {
                Map.Entry<String, String>[] entryArray = items.entrySet().toArray(new Map.Entry[items.entrySet().size()]);
                System.out.println(entryArray[i].getKey() + " - " + entryArray[i].getValue());
                itemsString.append(entryArray[i].getKey()).append(" - ").append(entryArray[i].getValue()).append("\n");
            }
//                resultTV.setText(result.toString());
            resultTV.setText(itemsString);
//            editScanner(new ArrayList<>(items.keySet()), new ArrayList<>(items.values()));
            isProcessing = false;
            RecyclerView itemsRC = findViewById(R.id.editScannerRecycler);
            GlobalResources.scannerItemAdapter = new ScannerItemAdapter(this);
            itemsRC.setLayoutManager(new LinearLayoutManager(this));
            itemsRC.setAdapter(GlobalResources.scannerItemAdapter);
            editScannerLayout.setVisibility(View.VISIBLE);
        }).addOnFailureListener(e -> {
            Toast.makeText(Receipt_Scanner.this, "Fail to detect text from image.." + e.getMessage(), Toast.LENGTH_SHORT).show();
            isProcessing = false;
        });
    }

    private TreeMap<String, String> analyzeText(ArrayList<String> inputArray, ArrayList<Rect> rectArray) {
        TreeMap<String, String> items = new TreeMap<>();
        ArrayList<String> namesOfItems = new ArrayList<>();
        ArrayList<String> prices = new ArrayList<>();
        ArrayList<Rect> nameRects = new ArrayList<>();
        ArrayList<Rect> priceRects = new ArrayList<>();
        StringBuffer tempString = new StringBuffer(100);
        StringBuffer tempInts = new StringBuffer(100);
        char lastLetter = ' ';
        boolean wasSuccessing = false;
        boolean wasDigiting = false;

        for (int j = 0; j < inputArray.size(); j++) {
            String inputText = inputArray.get(j);
            for (int i = 0; i < inputText.length(); i++) {
                if ((Character.isDigit(inputText.charAt(i))) || (inputText.charAt(i) == '.') || (inputText.charAt(i) == '$')) {
                    if (wasSuccessing && !tempString.toString().trim().isEmpty()) {
                        namesOfItems.add(tempString.toString());
                        nameRects.add(rectArray.get(j));
                        tempString = new StringBuffer(100);
                        wasSuccessing = false;
                        lastLetter = ' ';
                    }
                    tempInts.append(inputText.charAt(i));
                    wasDigiting = true;
//                if (inputText.charAt(i) == '.') {
//                    System.out.println("PERIOD: " + tempInts);
//                }
                } else {
                    if (wasDigiting) {
                        if (tempInts.toString().contains(".") && tempInts.toString().contains("$")) {
                            prices.add(tempInts.toString());
                            priceRects.add(rectArray.get(j));
                        }
                        tempInts = new StringBuffer(100);
                        wasDigiting = false;
                    }
                    if ((Character.isUpperCase(inputText.charAt(i))) || (Character.isLowerCase(inputText.charAt(i)))) {
                        wasSuccessing = true;
                        tempString.append(inputText.charAt(i));
//                    } else if (Character.isLowerCase(inputText.charAt(i))) {
//                        if (wasSuccessing && !tempString.toString().trim().isEmpty()) {
//                            namesOfItems.add(tempString.toString());
//                            nameRects.add(rectArray.get(j));
//                            tempString = new StringBuffer(100);
//                            wasSuccessing = false;
//                            lastLetter = ' ';
//                        }
                    } else if (inputText.charAt(i) == ' ') {
                        if (i < inputText.length() - 1) {
                            if (lastLetter != ' ' && !Character.isDigit(lastLetter) && inputText.charAt(i + 1) != ' ' && !Character.isDigit(inputText.charAt(i + 1))) {
                                wasSuccessing = true;
                                tempString.append(inputText.charAt(i));
                            } else {
                                if (wasSuccessing && !tempString.toString().trim().isEmpty()) {
                                    namesOfItems.add(tempString.toString());
                                    nameRects.add(rectArray.get(j));
                                    lastLetter = ' ';
                                }
                                tempString = new StringBuffer(100);
                                wasSuccessing = false;
                            }
                        }
                    }
                }
                lastLetter = inputText.charAt(i);
            }

            if (wasSuccessing && !tempString.toString().trim().isEmpty()) {
                namesOfItems.add(tempString.toString());
                nameRects.add(rectArray.get(j));
                tempString = new StringBuffer(100);
                wasSuccessing = false;
                lastLetter = ' ';
            }
            if (wasDigiting) {
                if (tempInts.toString().contains(".") && tempInts.toString().contains("$")) {
                    prices.add(tempInts.toString());
                    priceRects.add(rectArray.get(j));
                }
                tempInts = new StringBuffer(100);
                wasDigiting = false;
            }
        }

        for (String item : namesOfItems) {
            System.out.println("Item:" + item);
        }

        for (String item : prices) {
            System.out.println("Price: " + item);
        }

        if (namesOfItems.size() == prices.size()) {
            for (int i = 0; i < prices.size(); i++) {
                Rect prect = priceRects.get(i);
                TreeMap<Integer, Map.Entry<String, Rect>> listOfItems = new TreeMap<>();
                Rect irect;
                for (int j = 0; j < namesOfItems.size(); j++) {
                    irect = nameRects.get(j);
                    listOfItems.put(Math.abs(prect.centerY() - irect.centerY()), new SimpleEntry<>(namesOfItems.get(j), irect));
                }
                items.put(Objects.requireNonNull(listOfItems.get(listOfItems.firstKey())).getKey(), prices.get(i));
            }
        } else if (namesOfItems.size() < prices.size()) {
            for (int i = 0; i < namesOfItems.size(); i++) {
                Rect irect = nameRects.get(i);
                TreeMap<Integer, Map.Entry<String, Rect>> listOfItems = new TreeMap<>();
                for (int j = 0; j < prices.size(); j++) {
                    Rect prect = priceRects.get(i);
                    listOfItems.put(Math.abs(irect.centerY() - prect.centerY()), new SimpleEntry<>(prices.get(i), prect));
                }
                items.put(Objects.requireNonNull(listOfItems.get(listOfItems.firstKey())).getKey(), namesOfItems.get(i));
            }
        } else { // if (namesOfItems.size() > prices.size()) {
            for (int i = 0; i < prices.size(); i++) {
                Rect prect = priceRects.get(i);
                TreeMap<Integer, Map.Entry<String, Map.Entry<Rect, Rect>>> listOfItems = new TreeMap<>();
                Rect irect;
                for (int j = 0; j < namesOfItems.size(); j++) {
                    irect = nameRects.get(j);
                    listOfItems.put(Math.abs(prect.centerY() - irect.centerY()), new SimpleEntry<>(namesOfItems.get(j), new SimpleEntry<>(irect, prect)));
                }
                items.put(Objects.requireNonNull(listOfItems.get(listOfItems.firstKey())).getKey(), prices.get(i));
            }
        }

//        for (int i = 0; i < items.size(); i++) {
//            Map.Entry<String, String>[] entryArray = items.entrySet().toArray(new Map.Entry[items.entrySet().size()]);
//            System.out.println(": " + entryArray[i].getKey() + " - $" + entryArray[i].getValue());
//        }

        return items;
    }

    private void Draw(float leftx, float topy, float rightx, float bottomy) {
        Bitmap mutBitmap = Bitmap.createBitmap(imageBitmap.getWidth(), imageBitmap.getHeight(), imageBitmap.getConfig());
        Canvas canvas = new Canvas(mutBitmap);
        Paint paint = new Paint();
        paint.setColor(getColor(R.color.transparent_white));
        paint.setStyle(Paint.Style.FILL);
//        paint.setStrokeWidth(10);
        canvas.drawRect(leftx, topy, rightx, bottomy, paint);
        ImageView iv = new ImageView(getApplicationContext());
        iv.setImageBitmap(mutBitmap);
        frameLayout.addView(iv);
    }
}