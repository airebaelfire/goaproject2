package com.example.goaproject2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

public class DeleteItemDialogFragment extends DialogFragment {
    String itemName;

    public DeleteItemDialogFragment(String itemName) {
        this.itemName = itemName;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete " + itemName + "?")
                .setPositiveButton("Yes", (dialog, id) -> {
                    Receipt_Scanner.items.remove(itemName);
                    GlobalResources.scannerItemAdapter.notifyDataSetChanged();
                    Receipt_Scanner.items.forEach((name,price) -> System.out.println(name + price));
                })
                .setNegativeButton("No! Go back!", (dialog, id) -> {
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}