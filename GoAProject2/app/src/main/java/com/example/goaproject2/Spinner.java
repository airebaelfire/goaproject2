package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Spinner extends AppCompatActivity {

    public static String luckyPerson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);
        Button spinButton = (Button) findViewById(R.id.spinButton);
        Button finishButton = (Button) findViewById(R.id.finishButton);
        ImageButton backButton = (ImageButton) findViewById(R.id.backButton);
        TextView personPicked = (TextView) findViewById(R.id.personPicked);

        spinButton.setTransformationMethod(null);
        finishButton.setTransformationMethod(null);
        finishButton.setVisibility(View.GONE);

        ArrayList<Person> totalPeople = GlobalResources.people;
        HashMap<Integer, String> person = new HashMap<>();
        int min = 1;
        int max = 0;
        for (int k = 1; k <= totalPeople.size(); k++) {

            person.put(k, totalPeople.get(k - 1).name);
            max++;
        }

        int randomNumber = (int) (Math.random() * max + min);



        spinButton.setOnClickListener(v -> {

            //Spin
            luckyPerson = person.get(randomNumber);
            personPicked.setText(luckyPerson);
            //State Change
            spinButton.setVisibility(View.GONE);
            finishButton.setVisibility(View.VISIBLE);


        });

        backButton.setOnClickListener(v -> {
            //Go back
            finish();
        });

        finishButton.setOnClickListener(v -> {
            //Leave Spinner screen
            GlobalResources.people.get(randomNumber - 1).amount = GlobalResources.people.stream().mapToInt(personx -> personx.amount).sum();
            for (int i = 0; i < GlobalResources.people.size(); i++) {
                if (i == randomNumber - 1) {
                    continue;
                }
                GlobalResources.people.get(i).amount = 0;
            }
            Intent intent = new Intent(Spinner.this, FinalSummary.class);
            startActivity(intent);
        });
    }
}