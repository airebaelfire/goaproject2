package com.example.goaproject2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FinalPersonAdapter extends RecyclerView.Adapter<FinalPersonAdapter.ViewHolder>{
    ArrayList<Person> people;
    Context context;

    public FinalPersonAdapter(ArrayList<Person> people, Context context) {
        this.people = people;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView amount;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.final_name);
            amount = view.findViewById(R.id.final_amount);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.final_person, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.name.setText(GlobalResources.people.get(position).name);
        String amount = "$" + new DecimalFormat("#.00").format((double)GlobalResources.people.get(position).amount/100);
        viewHolder.amount.setText(amount);
    }

    @Override
    public int getItemCount() {
        return people.size();
    }
}
