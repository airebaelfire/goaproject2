package com.example.goaproject2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ScannerItemAdapter extends RecyclerView.Adapter<ScannerItemAdapter.ViewHolder> {
    ArrayList<String> names = new ArrayList<>(Receipt_Scanner.items.keySet());
    ArrayList<String> prices = new ArrayList<>(Receipt_Scanner.items.values());
    TreeMap<String, Person> withPeople = new TreeMap<>();
    Context context;
    private ArrayList<Person> people = new ArrayList<>(GlobalResources.people);
    private Person defaultPerson = new Person("Choose person", 0, new ArrayList<>(), false);

    public ScannerItemAdapter(Context context) {
        this.context = context;
        people.add(0, defaultPerson);
        Receipt_Scanner.items.forEach((name, price) -> withPeople.put(name, people.get(0)));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        EditText name;
        EditText price;
        Spinner person;
        Button done, delete;

        public ViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.item_name);
            price = view.findViewById(R.id.item_price);
            person = view.findViewById(R.id.item_person_spinner);
            done = view.findViewById(R.id.item_done_button);
            delete = view.findViewById(R.id.item_trashcan);
        }
    }

    @NonNull
    @Override
    public ScannerItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.scanner_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ScannerItemAdapter.ViewHolder viewHolder, int position) {
        names = new ArrayList<>(Receipt_Scanner.items.keySet());
        prices = new ArrayList<>(Receipt_Scanner.items.values());
        viewHolder.name.setText(names.get(position));
        viewHolder.price.setText(prices.get(position));
        ArrayAdapter<Person> adapter = new ArrayAdapter<>(context.getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, people);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewHolder.person.setAdapter(adapter);
        viewHolder.person.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                if (pos != 0) {
                    viewHolder.done.setEnabled(true);
                    viewHolder.done.setAlpha(1f);
                    withPeople.put(viewHolder.name.getText().toString(), people.get(pos));
                } else {
                    viewHolder.done.setEnabled(false);
                    viewHolder.done.setAlpha(0.5f);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewHolder.delete.setOnClickListener(v -> new DeleteItemDialogFragment(names.get(position)).show(((FragmentActivity)context).getSupportFragmentManager(), "delete"));
        viewHolder.done.setOnClickListener(v -> {
            if (withPeople.get(viewHolder.name.getText().toString()) != defaultPerson) {
                GlobalResources.people.get(GlobalResources.people.indexOf(withPeople.get(viewHolder.name.getText().toString()))).items.add(new Item(viewHolder.name.getText().toString(), Integer.parseInt(viewHolder.price.getText().toString().replaceAll("[^0-9]", ""))));
                Toast.makeText(context, "Added " + viewHolder.name.getText().toString(), Toast.LENGTH_SHORT).show();
                Receipt_Scanner.items.remove(names.get(position));
                notifyDataSetChanged();
                MainActivity.personAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Receipt_Scanner.items.size();
    }
}
