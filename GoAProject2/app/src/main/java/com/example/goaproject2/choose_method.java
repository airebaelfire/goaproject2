package com.example.goaproject2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class choose_method extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_method);

/*CHOOSE METHOD PAGE PROPERTIES*/

//      Properties
        Button back_button_choose = findViewById(R.id.back_button);
        TextView choose_method_page_title = findViewById(R.id.method_page_title);
        ImageView chicken_leg_bkg = findViewById(R.id.chicken_leg_background);

//      Average
        Button choose_average = findViewById(R.id.choose_average);
        TextView choose_average_text = findViewById(R.id.average_text);

//      Individual
        Button choose_individual = findViewById(R.id.choose_individual);
        TextView choose_individual_text = findViewById(R.id.individual_text);

//      By Share
        Button choose_by_share = findViewById(R.id.choose_by_share);
        TextView choose_by_share_text = findViewById(R.id.by_share_text);

//      One Lucky Person
        Button choose_one_lucky_person = findViewById(R.id.choose_one_lucky_person);
        TextView choose_one_lucky_person_text = findViewById(R.id.one_lucky_person_text);

/*CHOOSE METHOD PROPERTIES END*/


        back_button_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Back
                finish();
            }
        });

        choose_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Average
                Intent intent = new Intent(choose_method.this, Average.class);
                startActivity(intent);
            }
        });

        choose_individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Individual
                Intent intent = new Intent(choose_method.this, FinalSummary.class);
                startActivity(intent);
            }
        });

        choose_by_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open By Share
                Intent intent = new Intent(choose_method.this, ByShare.class);
                startActivity(intent);
            }
        });

        choose_one_lucky_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open One Lucky Person
                Intent intent = new Intent(choose_method.this, Spinner.class);
                startActivity(intent);
            }
        });
    }
}
