package com.example.goaproject2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class CustomAlertDialog extends DialogFragment {
    Person person;

    public CustomAlertDialog(Person person) {
        this.person = person;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.itemedit, null);
        @NonNull EditText name = v.findViewById(R.id.item_name);
        @NonNull EditText price = v.findViewById(R.id.item_price);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)price.getLayoutParams();
        params.setMarginEnd(10);
        price.setLayoutParams(params);
        builder.setView(v)
            // Add action buttons
            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    person.items.add(new Item(name.getText().toString(), Integer.parseInt(price.getText().toString().replaceAll("[^0-9]", ""))));
                    MainActivity.personAdapter.notifyDataSetChanged();
                }
            })
            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    CustomAlertDialog.this.getDialog().cancel();
                }
            });
        return builder.create();
    }
}
