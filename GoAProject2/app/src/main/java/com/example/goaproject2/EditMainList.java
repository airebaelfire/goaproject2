package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class EditMainList extends AppCompatActivity {
    boolean person1 = false;
    boolean person2 = false;
    boolean person3 = false;
    boolean person4 = false;
    boolean person5 = false;

    //        List 1
    ImageView list1;
    ImageView list1_1;
    EditText list_text1;
    ImageView trash_bkg1;
    ImageView trash1;

    //        List 2
    ImageView list2;
    ImageView list2_1;
    EditText list_text2;
    ImageView trash_bkg2;
    ImageView trash2;


    //        List 3
    ImageView list3;
    ImageView list3_1;
    EditText list_text3;
    ImageView trash_bkg3;
    ImageView trash3;


    //        List 4
    ImageView list4;
    ImageView list4_1;
    EditText list_text4;
    ImageView trash_bkg4;
    ImageView trash4;


    //        List 5
    ImageView list5;
    ImageView list5_1;
    EditText list_text5;
    ImageView trash_bkg5;
    ImageView trash5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_main_list);

        switch (GlobalResources.people.size()) {
            case 1:
                person1 = true;
                break;
            case 2:
                person1 = true;
                person2 = true;
                break;
            case 3:
                person1 = true;
                person2 = true;
                person3 = true;
                break;
            case 4:
                person1 = true;
                person2 = true;
                person3 = true;
                person4 = true;
                break;
            case 5:
                person1 = true;
                person2 = true;
                person3 = true;
                person4 = true;
                person5 = true;
                break;
        }


//      EDIT MAIN PAGE PROPERTIES START
        //        List 1
        list1 = findViewById(R.id.list1);
        list1_1 = findViewById(R.id.list1_1);
        list_text1 = findViewById(R.id.list_text1);
        trash_bkg1 = findViewById(R.id.trash_bkg);
        trash1 = findViewById(R.id.trash1);

        //        List 2
        list2 = findViewById(R.id.list2);
        list2_1 = findViewById(R.id.list2_1);
        list_text2 = findViewById(R.id.list_text2);
        trash_bkg2 = findViewById(R.id.trash_bkg2);
        trash2 = findViewById(R.id.trash2);


        //        List 3
        list3 = findViewById(R.id.list3);
        list3_1 = findViewById(R.id.list3_1);
        list_text3 = findViewById(R.id.list_text3);
        trash_bkg3 = findViewById(R.id.trash_bkg3);
        trash3 = findViewById(R.id.trash3);


        //        List 4
        list4 = findViewById(R.id.list4);
        list4_1 = findViewById(R.id.list4_1);
        list_text4 = findViewById(R.id.list_text4);
        trash_bkg4 = findViewById(R.id.trash_bkg4);
        trash4 = findViewById(R.id.trash4);


        //        List 5
        list5 = findViewById(R.id.list5);
        list5_1 = findViewById(R.id.list5_1);
        list_text5 = findViewById(R.id.list_text5);
        trash_bkg5 = findViewById(R.id.trash_bkg5);
        trash5 = findViewById(R.id.trash5);


        //        Add window properties
        ImageView add_window = findViewById(R.id.add_window);
        TextView add_window_text = findViewById(R.id.add_people_text);
        ImageView circle_1 = findViewById(R.id.circle1);
        ImageView circle_2 = findViewById(R.id.circle2);
        ImageView circle_3 = findViewById(R.id.circle3);
        ImageView circle_4 = findViewById(R.id.circle4);
        ImageView circle_5 = findViewById(R.id.circle5);
        TextView circle_text1 = findViewById(R.id.circle_text1);
        TextView circle_text2 = findViewById(R.id.circle_text2);
        TextView circle_text3 = findViewById(R.id.circle_text3);
        TextView circle_text4 = findViewById(R.id.circle_text4);
        TextView circle_text5 = findViewById(R.id.circle_text5);
        ImageView add_button = findViewById(R.id.add_button_cross);
        ImageView add_button_bkg = findViewById(R.id.add_button_bkg);

        //        Other Properties aka mascot, love that bird
        ImageView mascot = findViewById(R.id.mascot);

        //done button
        Button done = findViewById(R.id.done_button);

//        EDIT MAIN PAGE PROPERTIES END

        //Person 1
        list1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        list1_1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        list_text1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        list_text1.setText(person1 ? GlobalResources.people.get(0).name : "1");
        trash_bkg1.setVisibility(person1 ? View.VISIBLE : View.GONE);

        //Person 2
        list2.setVisibility(person2 ? View.VISIBLE : View.GONE);
        list2_1.setVisibility(person2 ? View.VISIBLE : View.GONE);
        list_text2.setVisibility(person2 ? View.VISIBLE : View.GONE);
        list_text2.setText(person2 ? GlobalResources.people.get(1).name : "2");
        trash_bkg2.setVisibility(person2 ? View.VISIBLE : View.GONE);

        //Person 3
        list3.setVisibility(person3 ? View.VISIBLE : View.GONE);
        list3_1.setVisibility(person3 ? View.VISIBLE : View.GONE);
        list_text3.setVisibility(person3 ? View.VISIBLE : View.GONE);
        list_text3.setText(person3 ? GlobalResources.people.get(2).name : "3");
        trash_bkg3.setVisibility(person3 ? View.VISIBLE : View.GONE);

        //Person 4
        list4.setVisibility(person4 ? View.VISIBLE : View.GONE);
        list4_1.setVisibility(person4 ? View.VISIBLE : View.GONE);
        list_text4.setVisibility(person4 ? View.VISIBLE : View.GONE);
        list_text4.setText(person4 ? GlobalResources.people.get(3).name : "4");
        trash_bkg4.setVisibility(person4 ? View.VISIBLE : View.GONE);

        //Person 5
        list5.setVisibility(person5 ? View.VISIBLE : View.GONE);
        list5_1.setVisibility(person5 ? View.VISIBLE : View.GONE);
        list_text5.setVisibility(person5 ? View.VISIBLE : View.GONE);
        list_text5.setText(person5 ? GlobalResources.people.get(4).name : "5");
        trash_bkg5.setVisibility(person5 ? View.VISIBLE : View.GONE);




        //      EDIT MAIN PAGE START
        //        List 1
        trash1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                person1 = false;
                Update();
            }
        });

        //        List 2
        trash2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                person2 = false;
                Update();
            }
        });

        //        List 3
        trash3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                person3 = false;
                Update();
            }
        });

        //        List 4
        trash4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                person4 = false;
                Update();
            }
        });

        //        List 5
        trash5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                person5 = false;
                Update();
            }
        });


        //        Add button function
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.VISIBLE));
                add_window_text.setVisibility((View.VISIBLE));
                circle_1.setVisibility((View.VISIBLE));
                circle_text1.setVisibility((View.VISIBLE));
                circle_2.setVisibility((View.VISIBLE));
                circle_text2.setVisibility((View.VISIBLE));
                circle_3.setVisibility((View.VISIBLE));
                circle_text3.setVisibility((View.VISIBLE));
                circle_4.setVisibility((View.VISIBLE));
                circle_text4.setVisibility((View.VISIBLE));
                circle_5.setVisibility((View.VISIBLE));
                circle_text5.setVisibility((View.VISIBLE));
            }
        });

        //        1
        circle_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                Update();
                person1 = true;

            }
        });

        //        2
        circle_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                Update();
                person1 = true;
                person2 = true;
            }
        });

        //        3
        circle_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                Update();
                person1 = true;
                person2 = true;
                person3 = true;

            }
        });

        //        4
        circle_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                Update();
                person1 = true;
                person2 = true;
                person3 = true;
                person4 = true;

            }
        });

        //        5
        circle_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                Update();
                person1 = true;
                person2 = true;
                person3 = true;
                person4 = true;
                person5 = true;

            }
        });

        //done button
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mascot.setVisibility(View.GONE);
//                add_window.setVisibility((View.GONE));
//                add_window_text.setVisibility((View.GONE));
//                circle_1.setVisibility((View.GONE));
//                circle_text1.setVisibility((View.GONE));
//                circle_2.setVisibility((View.GONE));
//                circle_text2.setVisibility((View.GONE));
//                circle_3.setVisibility((View.GONE));
//                circle_text3.setVisibility((View.GONE));
//                circle_4.setVisibility((View.GONE));
//                circle_text4.setVisibility((View.GONE));
//                circle_5.setVisibility((View.GONE));
//                circle_text5.setVisibility((View.GONE));
//                list1.setVisibility(View.GONE);
//                list1_1.setVisibility(View.GONE);
//                list_text1.setVisibility(View.GONE);
//                trash_bkg1.setVisibility(View.GONE);
//                list2.setVisibility(View.GONE);
//                list2_1.setVisibility(View.GONE);
//                list_text2.setVisibility(View.GONE);
//                trash_bkg2.setVisibility(View.GONE);
//                list3.setVisibility(View.GONE);
//                list3_1.setVisibility(View.GONE);
//                list_text3.setVisibility(View.GONE);
//                trash_bkg3.setVisibility(View.GONE);
//                list4.setVisibility(View.GONE);
//                list4_1.setVisibility(View.GONE);
//                list_text4.setVisibility(View.GONE);
//                trash_bkg4.setVisibility(View.GONE);
//                list5.setVisibility(View.GONE);
//                list5_1.setVisibility(View.GONE);
//                list_text5.setVisibility(View.GONE);
//                trash_bkg5.setVisibility(View.GONE);
//                add_button.setVisibility(View.GONE);
//                add_button_bkg.setVisibility(View.GONE);
                int totalPeople = person5 ? 5 : person4 ? 4 : person3 ? 3 : person2 ? 2 : person1 ? 1 : 1;
                if (GlobalResources.people.size() != totalPeople) {
                    if (GlobalResources.people.size() < totalPeople) {
                        for (int i = GlobalResources.people.size() + 1; i <= totalPeople; i++) {
                            TextView list;
                            switch (i) {
                                case 5:
                                    list = list_text5;
                                    break;
                                case 4:
                                    list = list_text4;
                                    break;
                                case 3:
                                    list = list_text3;
                                    break;
                                case 2:
                                    list = list_text2;
                                    break;
                                case 1:
                                    list = list_text1;
                                    break;
                                default:
                                    list = list_text1;
                                    break;
                            }
                            GlobalResources.people.add(new Person(list.getText().toString(), 0, new ArrayList<>(), false));
                        }
                        for (int i = 1; i <= GlobalResources.people.size(); i++) {
                            TextView list;
                            switch (i) {
                                case 5:
                                    list = list_text5;
                                    break;
                                case 4:
                                    list = list_text4;
                                    break;
                                case 3:
                                    list = list_text3;
                                    break;
                                case 2:
                                    list = list_text2;
                                    break;
                                case 1:
                                    list = list_text1;
                                    break;
                                default:
                                    list = list_text1;
                                    break;
                            }
                            GlobalResources.people.get(i - 1).name = list.getText().toString();
                        }
                    } else if (GlobalResources.people.size() > totalPeople) {
                        for (int i = GlobalResources.people.size(); i > totalPeople; i--) {
                            GlobalResources.people.remove(i - 1);
                        }
                        for (int i = 1; i <= GlobalResources.people.size(); i++) {
                            TextView list;
                            switch (i) {
                                case 5:
                                    list = list_text5;
                                    break;
                                case 4:
                                    list = list_text4;
                                    break;
                                case 3:
                                    list = list_text3;
                                    break;
                                case 2:
                                    list = list_text2;
                                    break;
                                case 1:
                                    list = list_text1;
                                    break;
                                default:
                                    list = list_text1;
                                    break;
                            }
                            GlobalResources.people.get(i - 1).name = list.getText().toString();
                        }
                    }
                } else {
                    for (int i = 1; i <= GlobalResources.people.size(); i++) {
                        TextView list;
                        switch (i) {
                            case 5:
                                list = list_text5;
                                break;
                            case 4:
                                list = list_text4;
                                break;
                            case 3:
                                list = list_text3;
                                break;
                            case 2:
                                list = list_text2;
                                break;
                            case 1:
                                list = list_text1;
                                break;
                            default:
                                list = list_text1;
                                break;
                        }
                        GlobalResources.people.get(i - 1).name = list.getText().toString();
                    }
                }
                MainActivity.personAdapter.notifyDataSetChanged();
                finish();
            }
        });

    }
    public void Update() {
        //Person 1
        list1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        list1_1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        list_text1.setVisibility(person1 ? View.VISIBLE : View.GONE);
        trash_bkg1.setVisibility(person1 ? View.VISIBLE : View.GONE);

        //Person 2
        list2.setVisibility(person2 ? View.VISIBLE : View.GONE);
        list2_1.setVisibility(person2 ? View.VISIBLE : View.GONE);
        list_text2.setVisibility(person2 ? View.VISIBLE : View.GONE);
        trash_bkg2.setVisibility(person2 ? View.VISIBLE : View.GONE);

        //Person 3
        list3.setVisibility(person3 ? View.VISIBLE : View.GONE);
        list3_1.setVisibility(person3 ? View.VISIBLE : View.GONE);
        list_text3.setVisibility(person3 ? View.VISIBLE : View.GONE);
        trash_bkg3.setVisibility(person3 ? View.VISIBLE : View.GONE);

        //Person 4
        list4.setVisibility(person4 ? View.VISIBLE : View.GONE);
        list4_1.setVisibility(person4 ? View.VISIBLE : View.GONE);
        list_text4.setVisibility(person4 ? View.VISIBLE : View.GONE);
        trash_bkg4.setVisibility(person4 ? View.VISIBLE : View.GONE);

        //Person 5
        list5.setVisibility(person5 ? View.VISIBLE : View.GONE);
        list5_1.setVisibility(person5 ? View.VISIBLE : View.GONE);
        list_text5.setVisibility(person5 ? View.VISIBLE : View.GONE);
        trash_bkg5.setVisibility(person5 ? View.VISIBLE : View.GONE);
    }
}