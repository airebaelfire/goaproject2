package com.example.goaproject2;

import androidx.appcompat.app.AppCompatActivity;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



public class LeciaClassTemp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        AVERAGE PAGE
//        Properties
        ImageView back_button_average = findViewById(R.id.back_button_average) ;
        ImageView legs = findViewById(R.id.legs) ;
        TextView title_average =  findViewById(R.id.title_average) ;
        ImageView blue_rect = findViewById(R.id.blue_rect) ;
        ImageView grey_rect = findViewById(R.id.gray_rect) ;
        TextView total_amount_text =  findViewById(R.id.total_amount) ;
        ImageView lit_rect_1 = findViewById(R.id.lit_rect1) ;
        ImageView lit_rect2 = findViewById(R.id.lit_rect2) ;
        ImageView dot_average = findViewById(R.id.dot) ;
        ImageView finish_button_average = findViewById(R.id.finish_button_average);
        TextView finish_button_text_average = findViewById(R.id.finish_text_average) ;
        ImageView arrow_average = findViewById(R.id.arrow_average) ;
        EditText edit_number_average_1 = findViewById(R.id.edit_number_average_1);
        EditText edit_number_average_2 = findViewById(R.id.edit_number_average_2);
        TextView dollar_sign_average = findViewById(R.id.dollar_sign_average) ;







//          CHOOSE METHOD PAGE PROPERTIES
//        Properties
        Button back_button_choose = findViewById(R.id.back_button);
        TextView choose_method_page_title =  findViewById(R.id.method_page_title);
        ImageView chicken_leg_bkg =  findViewById(R.id.chicken_leg_background);

        //        Average
        Button choose_average =  findViewById(R.id.choose_average);
        TextView choose_average_text =  findViewById(R.id.average_text);

//              Individual
        Button choose_individual = findViewById(R.id.choose_individual);
        TextView choose_individual_text = findViewById(R.id.individual_text);

//              By Share
        Button choose_by_share =  findViewById(R.id.choose_by_share);
        TextView choose_by_share_text = findViewById(R.id.by_share_text);

//              One Lucky Person
        Button choose_one_lucky_person =findViewById(R.id.choose_one_lucky_person);
        TextView choose_one_lucky_person_text=  findViewById(R.id.one_lucky_person_text);
//CHOOSE METHOD PROPERTIES END





//      EDIT MAIN PAGE PROPERTIES START
        //        List 1
        ImageView list1 = findViewById(R.id.list1);
        ImageView list1_1 = findViewById(R.id.list1_1);
        TextView list_text1 = findViewById(R.id.list_text1);
        ImageView trash_bkg1 = findViewById(R.id.trash_bkg);
        ImageView trash1 =  findViewById(R.id.trash1);

        //        List 2
        ImageView list2 = findViewById(R.id.list2);
        ImageView list2_1 = findViewById(R.id.list2_1);
        TextView list_text2 =  findViewById(R.id.list_text2);
        ImageView trash_bkg2 = findViewById(R.id.trash_bkg2);
        ImageView trash2 = findViewById(R.id.trash2);


        //        List 3
        ImageView list3 =  findViewById(R.id.list3);
        ImageView list3_1 =  findViewById(R.id.list3_1);
        TextView list_text3 = findViewById(R.id.list_text3);
        ImageView trash_bkg3 = findViewById(R.id.trash_bkg3);
        ImageView trash3 = findViewById(R.id.trash3);


        //        List 4
        ImageView list4 = findViewById(R.id.list4);
        ImageView list4_1 =  findViewById(R.id.list4_1);
        TextView list_text4 = findViewById(R.id.list_text4);
        ImageView trash_bkg4 = findViewById(R.id.trash_bkg4);
        ImageView trash4 = findViewById(R.id.trash4);


        //        List 5
        ImageView list5 = findViewById(R.id.list5);
        ImageView list5_1 =  findViewById(R.id.list5_1);
        TextView list_text5 = findViewById(R.id.list_text5);
        ImageView trash_bkg5 = findViewById(R.id.trash_bkg5);
        ImageView trash5 =  findViewById(R.id.trash5);


        //        Add window properties
        ImageView add_window =  findViewById(R.id.add_window);
        TextView add_window_text = findViewById(R.id.add_people_text);
        ImageView circle_1 =  findViewById(R.id.circle1);
        ImageView circle_2 =  findViewById(R.id.circle2);
        ImageView circle_3 =  findViewById(R.id.circle3);
        ImageView circle_4 = findViewById(R.id.circle4);
        ImageView circle_5 =  findViewById(R.id.circle5);
        TextView circle_text1 = findViewById(R.id.circle_text1);
        TextView circle_text2 =  findViewById(R.id.circle_text2);
        TextView circle_text3 =findViewById(R.id.circle_text3);
        TextView circle_text4 =  findViewById(R.id.circle_text4);
        TextView circle_text5 = findViewById(R.id.circle_text5);
        ImageView add_button = findViewById(R.id.add_button_cross);
        ImageView add_button_bkg = findViewById(R.id.add_button_bkg);

        //        Other Properties aka mascot, love that bird
        ImageView mascot =  findViewById(R.id.mascot);

        //done button
        Button done =  findViewById(R.id.done_button);

//        EDIT MAIN PAGE PROPERTIES END





//AVERAGE PAGE START
        back_button_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AVERAGE PROPERTIES
                back_button_average.setVisibility(View.GONE);
                legs.setVisibility(View.GONE);
                title_average.setVisibility(View.GONE);
                grey_rect.setVisibility(View.GONE);
                blue_rect.setVisibility(View.GONE);
                total_amount_text.setVisibility(View.GONE);
                lit_rect2.setVisibility(View.GONE);
                lit_rect_1.setVisibility(View.GONE);
                dot_average.setVisibility(View.GONE);
                finish_button_average.setVisibility(View.GONE);
                finish_button_text_average.setVisibility(View.GONE);
                arrow_average.setVisibility(View.GONE);
                edit_number_average_1.setVisibility(View.GONE);
                edit_number_average_2.setVisibility(View.GONE);
                dollar_sign_average.setVisibility(View.GONE);

//                CHOOSE METHOD PROPERTIES
                back_button_choose.setVisibility(View.VISIBLE);
                choose_method_page_title.setVisibility(View.VISIBLE);
                chicken_leg_bkg.setVisibility(View.VISIBLE);
                choose_average.setVisibility(View.VISIBLE);
                choose_average_text.setVisibility(View.VISIBLE);
                choose_individual.setVisibility(View.VISIBLE);
                choose_individual_text.setVisibility(View.VISIBLE);
                choose_by_share.setVisibility(View.VISIBLE);
                choose_by_share_text.setVisibility(View.VISIBLE);
                choose_one_lucky_person.setVisibility(View.VISIBLE);
                choose_one_lucky_person_text.setVisibility(View.VISIBLE);

            }
        });

        finish_button_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_button_average.setVisibility(View.GONE);
                legs.setVisibility(View.GONE);
                title_average.setVisibility(View.GONE);
                grey_rect.setVisibility(View.GONE);
                blue_rect.setVisibility(View.GONE);
                total_amount_text.setVisibility(View.GONE);
                lit_rect2.setVisibility(View.GONE);
                lit_rect_1.setVisibility(View.GONE);
                dot_average.setVisibility(View.GONE);
                finish_button_average.setVisibility(View.GONE);
                finish_button_text_average.setVisibility(View.GONE);
                arrow_average.setVisibility(View.GONE);
                edit_number_average_1.setVisibility(View.GONE);
                edit_number_average_2.setVisibility(View.GONE);
                dollar_sign_average.setVisibility(View.GONE);


            }
        });
//          AVERAGE PAGE END






//          CHOOSE METHOD PAGE
        back_button_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_button_choose.setVisibility(View.GONE);
                choose_method_page_title.setVisibility(View.GONE);
                chicken_leg_bkg.setVisibility(View.GONE);
                choose_average.setVisibility(View.GONE);
                choose_average_text.setVisibility(View.GONE);
                choose_individual.setVisibility(View.GONE);
                choose_individual_text.setVisibility(View.GONE);
                choose_by_share.setVisibility(View.GONE);
                choose_by_share_text.setVisibility(View.GONE);
                choose_one_lucky_person.setVisibility(View.GONE);
                choose_one_lucky_person_text.setVisibility(View.GONE);


            }
        });

        choose_average.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //              ALL CHOOSE METHOD PROPERTIES
                back_button_choose.setVisibility(View.GONE);
                choose_method_page_title.setVisibility(View.GONE);
                chicken_leg_bkg.setVisibility(View.GONE);
                choose_average.setVisibility(View.GONE);
                choose_average_text.setVisibility(View.GONE);
                choose_individual.setVisibility(View.GONE);
                choose_individual_text.setVisibility(View.GONE);
                choose_by_share.setVisibility(View.GONE);
                choose_by_share_text.setVisibility(View.GONE);
                choose_one_lucky_person.setVisibility(View.GONE);
                choose_one_lucky_person_text.setVisibility(View.GONE);

//              ALL AVERAGE PROPERTIES
                back_button_average.setVisibility(View.VISIBLE);
                legs.setVisibility(View.VISIBLE);
                title_average.setVisibility(View.VISIBLE);
                lit_rect2.setVisibility(View.VISIBLE);
                lit_rect_1.setVisibility(View.VISIBLE);
                blue_rect.setVisibility(View.VISIBLE);
                grey_rect.setVisibility(View.VISIBLE);
                finish_button_average.setVisibility(View.VISIBLE);
                finish_button_text_average.setVisibility(View.VISIBLE);
                dot_average.setVisibility(View.VISIBLE);
                total_amount_text.setVisibility(View.VISIBLE);
                arrow_average.setVisibility(View.VISIBLE);
                edit_number_average_1.setVisibility(View.VISIBLE);
                edit_number_average_2.setVisibility(View.VISIBLE);
                dollar_sign_average.setVisibility(View.VISIBLE);

            }
        });

        choose_individual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_button_choose.setVisibility(View.GONE);
                choose_method_page_title.setVisibility(View.GONE);
                chicken_leg_bkg.setVisibility(View.GONE);
                choose_average.setVisibility(View.GONE);
                choose_average_text.setVisibility(View.GONE);
                choose_individual.setVisibility(View.GONE);
                choose_individual_text.setVisibility(View.GONE);
                choose_by_share.setVisibility(View.GONE);
                choose_by_share_text.setVisibility(View.GONE);
                choose_one_lucky_person.setVisibility(View.GONE);
                choose_one_lucky_person_text.setVisibility(View.GONE);
            }
        });

        choose_by_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_button_choose.setVisibility(View.GONE);
                choose_method_page_title.setVisibility(View.GONE);
                chicken_leg_bkg.setVisibility(View.GONE);
                choose_average.setVisibility(View.GONE);
                choose_average_text.setVisibility(View.GONE);
                choose_individual.setVisibility(View.GONE);
                choose_individual_text.setVisibility(View.GONE);
                choose_by_share.setVisibility(View.GONE);
                choose_by_share_text.setVisibility(View.GONE);
                choose_one_lucky_person.setVisibility(View.GONE);
                choose_one_lucky_person_text.setVisibility(View.GONE);
            }
        });

        choose_one_lucky_person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_button_choose.setVisibility(View.GONE);
                choose_method_page_title.setVisibility(View.GONE);
                chicken_leg_bkg.setVisibility(View.GONE);
                choose_average.setVisibility(View.GONE);
                choose_average_text.setVisibility(View.GONE);
                choose_individual.setVisibility(View.GONE);
                choose_individual_text.setVisibility(View.GONE);
                choose_by_share.setVisibility(View.GONE);
                choose_by_share_text.setVisibility(View.GONE);
                choose_one_lucky_person.setVisibility(View.GONE);
                choose_one_lucky_person_text.setVisibility(View.GONE);
            }
        });

//      CHOOSE METHOD PAGE END








//      EDIT MAIN PAGE START
        //        List 1
        trash1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list1.setVisibility(View.GONE);
                list1_1.setVisibility(View.GONE);
                list_text1.setVisibility(View.GONE);
                trash_bkg1.setVisibility(View.GONE);
            }
        });

        //        List 2
        trash2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list2.setVisibility(View.GONE);
                list2_1.setVisibility(View.GONE);
                list_text2.setVisibility(View.GONE);
                trash_bkg2.setVisibility(View.GONE);
            }
        });

        //        List 3
        trash3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list3.setVisibility(View.GONE);
                list3_1.setVisibility(View.GONE);
                list_text3.setVisibility(View.GONE);
                trash_bkg3.setVisibility(View.GONE);
            }
        });

        //        List 4
        trash4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list4.setVisibility(View.GONE);
                list4_1.setVisibility(View.GONE);
                list_text4.setVisibility(View.GONE);
                trash_bkg4.setVisibility(View.GONE);
            }
        });

        //        List 5
        trash5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list5.setVisibility(View.GONE);
                list5_1.setVisibility(View.GONE);
                list_text5.setVisibility(View.GONE);
                trash_bkg5.setVisibility(View.GONE);
            }
        });


        //        Add button function
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.VISIBLE));
                add_window_text.setVisibility((View.VISIBLE));
                circle_1.setVisibility((View.VISIBLE));
                circle_text1.setVisibility((View.VISIBLE));
                circle_2.setVisibility((View.VISIBLE));
                circle_text2.setVisibility((View.VISIBLE));
                circle_3.setVisibility((View.VISIBLE));
                circle_text3.setVisibility((View.VISIBLE));
                circle_4.setVisibility((View.VISIBLE));
                circle_text4.setVisibility((View.VISIBLE));
                circle_5.setVisibility((View.VISIBLE));
                circle_text5.setVisibility((View.VISIBLE));
            }
        });

        //        1
        circle_1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.VISIBLE);
                list1_1.setVisibility(View.VISIBLE);
                list_text1.setVisibility(View.VISIBLE);
                trash_bkg1.setVisibility(View.VISIBLE);

            }
        });

        //        2
        circle_2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.VISIBLE);
                list1_1.setVisibility(View.VISIBLE);
                list_text1.setVisibility(View.VISIBLE);
                trash_bkg1.setVisibility(View.VISIBLE);
                list2.setVisibility(View.VISIBLE);
                list2_1.setVisibility(View.VISIBLE);
                list_text2.setVisibility(View.VISIBLE);
                trash_bkg2.setVisibility(View.VISIBLE);

            }
        });

        //        3
        circle_3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.VISIBLE);
                list1_1.setVisibility(View.VISIBLE);
                list_text1.setVisibility(View.VISIBLE);
                trash_bkg1.setVisibility(View.VISIBLE);
                list2.setVisibility(View.VISIBLE);
                list2_1.setVisibility(View.VISIBLE);
                list_text2.setVisibility(View.VISIBLE);
                trash_bkg2.setVisibility(View.VISIBLE);
                list3.setVisibility(View.VISIBLE);
                list3_1.setVisibility(View.VISIBLE);
                list_text3.setVisibility(View.VISIBLE);
                trash_bkg3.setVisibility(View.VISIBLE);

            }
        });

        //        4
        circle_4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.VISIBLE);
                list1_1.setVisibility(View.VISIBLE);
                list_text1.setVisibility(View.VISIBLE);
                trash_bkg1.setVisibility(View.VISIBLE);
                list2.setVisibility(View.VISIBLE);
                list2_1.setVisibility(View.VISIBLE);
                list_text2.setVisibility(View.VISIBLE);
                trash_bkg2.setVisibility(View.VISIBLE);
                list3.setVisibility(View.VISIBLE);
                list3_1.setVisibility(View.VISIBLE);
                list_text3.setVisibility(View.VISIBLE);
                trash_bkg3.setVisibility(View.VISIBLE);
                list4.setVisibility(View.VISIBLE);
                list4_1.setVisibility(View.VISIBLE);
                list_text4.setVisibility(View.VISIBLE);
                trash_bkg4.setVisibility(View.VISIBLE);

            }
        });

        //        5
        circle_5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.VISIBLE);
                list1_1.setVisibility(View.VISIBLE);
                list_text1.setVisibility(View.VISIBLE);
                trash_bkg1.setVisibility(View.VISIBLE);
                list2.setVisibility(View.VISIBLE);
                list2_1.setVisibility(View.VISIBLE);
                list_text2.setVisibility(View.VISIBLE);
                trash_bkg2.setVisibility(View.VISIBLE);
                list3.setVisibility(View.VISIBLE);
                list3_1.setVisibility(View.VISIBLE);
                list_text3.setVisibility(View.VISIBLE);
                trash_bkg3.setVisibility(View.VISIBLE);
                list4.setVisibility(View.VISIBLE);
                list4_1.setVisibility(View.VISIBLE);
                list_text4.setVisibility(View.VISIBLE);
                trash_bkg4.setVisibility(View.VISIBLE);
                list5.setVisibility(View.VISIBLE);
                list5_1.setVisibility(View.VISIBLE);
                list_text5.setVisibility(View.VISIBLE);
                trash_bkg5.setVisibility(View.VISIBLE);

            }
        });

        //done button
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mascot.setVisibility(View.GONE);
                add_window.setVisibility((View.GONE));
                add_window_text.setVisibility((View.GONE));
                circle_1.setVisibility((View.GONE));
                circle_text1.setVisibility((View.GONE));
                circle_2.setVisibility((View.GONE));
                circle_text2.setVisibility((View.GONE));
                circle_3.setVisibility((View.GONE));
                circle_text3.setVisibility((View.GONE));
                circle_4.setVisibility((View.GONE));
                circle_text4.setVisibility((View.GONE));
                circle_5.setVisibility((View.GONE));
                circle_text5.setVisibility((View.GONE));
                list1.setVisibility(View.GONE);
                list1_1.setVisibility(View.GONE);
                list_text1.setVisibility(View.GONE);
                trash_bkg1.setVisibility(View.GONE);
                list2.setVisibility(View.GONE);
                list2_1.setVisibility(View.GONE);
                list_text2.setVisibility(View.GONE);
                trash_bkg2.setVisibility(View.GONE);
                list3.setVisibility(View.GONE);
                list3_1.setVisibility(View.GONE);
                list_text3.setVisibility(View.GONE);
                trash_bkg3.setVisibility(View.GONE);
                list4.setVisibility(View.GONE);
                list4_1.setVisibility(View.GONE);
                list_text4.setVisibility(View.GONE);
                trash_bkg4.setVisibility(View.GONE);
                list5.setVisibility(View.GONE);
                list5_1.setVisibility(View.GONE);
                list_text5.setVisibility(View.GONE);
                trash_bkg5.setVisibility(View.GONE);
                add_button.setVisibility(View.GONE);
                add_button_bkg.setVisibility(View.GONE);
            }
        });

        //      EDIT MAIN PAGE END





    }
}