package com.example.goaproject2;

import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class shareAdapter extends RecyclerView.Adapter<shareAdapter.ViewHolder> {
    ArrayList<Person> data = GlobalResources.people;
    public static ArrayList<String> numerator;
    public static ArrayList<String> denominator;
    Button finish_button;

    public shareAdapter(Button finish_button) {

        numerator = new ArrayList<>(Collections.nCopies(GlobalResources.people.size(), ""));
        denominator = new ArrayList<>(Collections.nCopies(GlobalResources.people.size(), ""));
        this.finish_button = finish_button;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView number;
        TextView name;
        EditText numerator;
        EditText denominator;

        public ViewHolder(View view) {

            super(view);

            number = view.findViewById(R.id.one);
            name = view.findViewById(R.id.firstName);
            numerator = view.findViewById(R.id.numerator);
            denominator = view.findViewById(R.id.denominator);
        }
    }

    @NonNull
    @Override
    public shareAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.share, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull shareAdapter.ViewHolder holder, int position) {

        holder.name.setText(data.get(position).name);
        holder.number.setText(Integer.toString(position + 1));
        holder.numerator.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int index = Integer.parseInt(holder.number.getText().toString());
                numerator.set(index - 1, s.toString());
            }
        });
        holder.numerator.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                finish_button.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
            }
        });
        holder.numerator.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    finish_button.setVisibility(View.VISIBLE);
                } return false;
            }
        });

        holder.denominator.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int index = Integer.parseInt(holder.number.getText().toString());
                denominator.set(index - 1, s.toString());

            }
        });
        holder.denominator.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                finish_button.setVisibility(hasFocus ? View.GONE : View.VISIBLE);
            }
        });
        holder.denominator.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    finish_button.setVisibility(View.VISIBLE);
                } return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
